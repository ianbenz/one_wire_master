// Copyright Ian Benz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Module: one_wire_master_controller
//
// Manages the generator to perform bus reset, write bit, write byte,
// and triplet operations.
//
// Ports:
//
//   clk_div - 1-Wire clock divider. Must remain valid for the entire operation.
//
//   start - Start an operation when the controller is idle.
//
//   operation - Select bus reset (0), write bit (1), write byte (2),
//               or triplet (3).
//
//   overdrive - Set to select overdrive timing or clear to select standard
//               timing. Must remain valid for the entire operation.
//
//   spu_en - Enable SPU when the controller is idle.
//
//   apu_en - Enable APU. Must remain valid for the entire operation.
//
//   param - Value to transmit when write bit, write byte, or triplet is
//           selected. Only bit 0 is used for write bit and triplet.
//           Must remain valid for the entire operation.
//
//   done - Asserted for one clock cyle to indicate that the operation is done
//          and the controller is idle.
//
//   result - For bus reset, bit 0 indicates presence detected and bit 1
//            indicates short detected. For write bit, bit 0 indicates the
//            sampled value. For triplet, bit 2 indicates the read bit, bit 1
//            indicates the read complement (second) bit, and bit 0 indicates
//            the written bit. Only valid when the operation is done and the
//            controller is idle.
module one_wire_master_controller (
  // Controller interface
  input logic clk,
  input logic reset,
  input logic [7:0] clk_div,
  input logic start,
  input logic [1:0] operation,
  input logic overdrive,
  input logic spu_en,
  input logic apu_en,
  input logic [7:0] param,
  output logic done,
  output logic [7:0] result,

  // 1-Wire interface
  input logic sample,
  input logic apu,
  output logic drive,
  output logic spu
);
  localparam bit [1:0] bus_reset_operation = 0,
                       write_bit_operation = 1,
                       write_byte_operation = 2,
                       triplet_operation = 3;

  logic generator_start;
  logic generator_bus_reset;
  logic generator_spu_en;
  logic generator_param;
  logic generator_done;
  logic [1:0] generator_result;

  enum logic [3:0] {
    idle,
    bus_reset,
    write_bit,
    write_byte_bit_0,
    write_byte_bit_1,
    write_byte_bit_2,
    write_byte_bit_3,
    write_byte_bit_4,
    write_byte_bit_5,
    write_byte_bit_6,
    write_byte_bit_7,
    triplet_read_bit,
    triplet_read_bit_complement,
    triplet_write_bit
  } state;

  one_wire_master_generator generator (
    .start(generator_start),
    .bus_reset(generator_bus_reset),
    .spu_en(generator_spu_en),
    .param(generator_param),
    .done(generator_done),
    .result(generator_result),
    .*
  );

  always_ff @(posedge clk or posedge reset) begin
    done <= '0;
    generator_start <= '0;

    if (reset) begin
      generator_spu_en <= '0;
      state <= idle;
    end else begin
      generator_spu_en <= spu_en;

      unique case (state)
        idle: begin
          if (start) begin
            generator_start <= '1;

            unique case (operation)
              bus_reset_operation: state <= bus_reset;
              write_bit_operation: state <= write_bit;
              write_byte_operation: state <= write_byte_bit_0;
              triplet_operation: state <= triplet_read_bit;
            endcase
          end
        end

        write_byte_bit_0: begin
          generator_start <= '1;

          if (generator_done) begin
            state <= write_byte_bit_1;
          end
        end

        write_byte_bit_1: begin
          generator_start <= '1;

          if (generator_done) begin
            state <= write_byte_bit_2;
          end
        end

        write_byte_bit_2: begin
          generator_start <= '1;

          if (generator_done) begin
            state <= write_byte_bit_3;
          end
        end

        write_byte_bit_3: begin
          generator_start <= '1;

          if (generator_done) begin
            state <= write_byte_bit_4;
          end
        end

        write_byte_bit_4: begin
          generator_start <= '1;

          if (generator_done) begin
            state <= write_byte_bit_5;
          end
        end

        write_byte_bit_5: begin
          generator_start <= '1;

          if (generator_done) begin
            state <= write_byte_bit_6;
          end
        end

        write_byte_bit_6: begin
          generator_start <= '1;

          if (generator_done) begin
            state <= write_byte_bit_7;
          end
        end

        triplet_read_bit: begin
          generator_start <= '1;

          if (generator_done) begin
            state <= triplet_read_bit_complement;
          end
        end

        triplet_read_bit_complement: begin
          // Disable SPU since generator stops while determining write bit.
          generator_spu_en <= '0;

          if (generator_done) begin
            generator_start <= '1;
            state <= triplet_write_bit;
          end
        end

        default: begin
          if (generator_done) begin
            done <= '1;
            state <= idle;
          end
        end
      endcase
    end
  end

  // Configure generator for next state.
  always_ff @(posedge clk) begin
    generator_bus_reset <= '0;
    generator_param <= '0;

    unique0 case (state)
      idle: begin
        unique case (operation)
          bus_reset_operation: generator_bus_reset <= '1;
          write_bit_operation,
          write_byte_operation: generator_param <= param[0];
          triplet_operation: generator_param <= '1;
        endcase
      end

      write_byte_bit_0: generator_param <= param[1];
      write_byte_bit_1: generator_param <= param[2];
      write_byte_bit_2: generator_param <= param[3];
      write_byte_bit_3: generator_param <= param[4];
      write_byte_bit_4: generator_param <= param[5];
      write_byte_bit_5: generator_param <= param[6];
      write_byte_bit_6: generator_param <= param[7];

      triplet_read_bit: generator_param <= '1;
      triplet_read_bit_complement: begin
        if (result[2]) begin
          generator_param <= '1;
        end else if (generator_result[0]) begin
          generator_param <= '0;
        end else begin
          generator_param <= param[0];
        end
      end
    endcase
  end

  always_ff @(posedge clk) begin
    unique0 case (state)
      bus_reset: result <= {6'b0, generator_result};

      write_bit: result <= {7'b0, generator_result[0]};

      write_byte_bit_0: result[0] <= generator_result[0];
      write_byte_bit_1: result[1] <= generator_result[0];
      write_byte_bit_2: result[2] <= generator_result[0];
      write_byte_bit_3: result[3] <= generator_result[0];
      write_byte_bit_4: result[4] <= generator_result[0];
      write_byte_bit_5: result[5] <= generator_result[0];
      write_byte_bit_6: result[6] <= generator_result[0];
      write_byte_bit_7: result[7] <= generator_result[0];

      triplet_read_bit: result[2] <= generator_result[0];
      triplet_read_bit_complement: result[1] <= generator_result[0];
      triplet_write_bit: result <= {5'b0, result[2:1], generator_result[0]};
    endcase
  end
endmodule

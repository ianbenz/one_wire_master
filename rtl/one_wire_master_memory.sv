// Copyright Ian Benz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Module: one_wire_master_memory
// Manages the peripheral interface and controller.
module one_wire_master_memory (
  // Peripheral and controller interfaces
  input logic clk,
  input logic reset,

  // Peripheral interface
  input logic enable,
  input logic write,
  input logic [1:0] addr,
  input logic [7:0] wdata,
  output logic [7:0] rdata,
  output logic irq,

  // Controller interface
  output logic [7:0] clk_div,
  output logic start,
  output logic [1:0] operation,
  output logic overdrive,
  output logic spu_en,
  output logic apu_en,
  output logic [7:0] param,
  input logic done,
  input logic [7:0] result
);
  // Data addresses
  localparam bit [1:0] ctrl_addr = 0,
                       param_addr = 1,
                       result_addr = 2,
                       clk_div_addr = 3;

  // Control bit addresses
  localparam bit [2:0] ctrl_operation_en = 7,
                       ctrl_operation = 5,
                       ctrl_overdrive = 4,
                       ctrl_spu = 3,
                       ctrl_apu = 2,
                       ctrl_irq_en = 1,
                       ctrl_irq = 0;

  // Data registers
  logic [7:0] data[4];

  always_ff @(posedge clk) begin
    if (enable && !write) begin
      rdata <= data[addr];
    end
  end

  assign irq = data[ctrl_addr][ctrl_irq_en] & data[ctrl_addr][ctrl_irq];
  assign clk_div = data[clk_div_addr];
  assign operation = data[ctrl_addr][ctrl_operation +: $size(operation)];
  assign overdrive = data[ctrl_addr][ctrl_overdrive];
  assign spu_en = data[ctrl_addr][ctrl_spu];
  assign apu_en = data[ctrl_addr][ctrl_apu];
  assign param = data[param_addr];

  always_ff @(posedge clk or posedge reset) begin
    start <= '0;

    if (reset) begin
      foreach (data[addr]) begin
        data[addr] <= '0;
      end
    end else if (data[ctrl_addr][ctrl_operation_en]) begin
      // Handle peripheral writes.
      if (enable && write && addr == ctrl_addr) begin
        data[ctrl_addr][ctrl_irq_en] <= wdata[ctrl_irq_en];
        data[ctrl_addr][ctrl_irq] <=
            data[ctrl_addr][ctrl_irq] & wdata[ctrl_irq];
      end

      // Handle controller writes.
      if (done) begin
        data[ctrl_addr][ctrl_operation_en] <= '0;
        data[ctrl_addr][ctrl_irq] <= '1;
        data[result_addr] <= result;
      end
    end else if (enable && write) begin
      unique case (addr)
        ctrl_addr: begin
          start <= wdata[ctrl_operation_en];

          // IRQ can only be set by controller.
          data[ctrl_addr] <= wdata;
          data[ctrl_addr][ctrl_irq] <=
              data[ctrl_addr][ctrl_irq] & wdata[ctrl_irq];
        end

        result_addr: ; // Read-only

        default: data[addr] <= wdata;
      endcase
    end
  end
endmodule

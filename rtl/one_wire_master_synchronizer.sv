// Copyright Ian Benz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Module: one_wire_master_synchronizer
// Two-stage flip-flop synchronizer.
module one_wire_master_synchronizer (
  input logic clk,
  input logic in,
  output logic out
);
  logic buffer;

  always_ff @(posedge clk) begin
    out <= buffer;
    buffer <= in;
  end
endmodule

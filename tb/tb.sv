// Copyright Ian Benz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

module tb;
  import tests_pkg::*;
  import uvm_pkg::*;

  apb_agent_if apb ();
  gpio_agent_if irq ();
  one_wire_agent_if one_wire ();

  one_wire_master_top dut (
    .pclk(apb.pclk),
    .preset_n(apb.preset_n),
    .paddr(apb.paddr[1:0]),
    .psel(apb.psel),
    .penable(apb.penable),
    .pwrite(apb.pwrite),
    .pwdata(apb.pwdata[7:0]),
    .pready(apb.pready),
    .prdata(apb.prdata[7:0]),
    .irq(irq.value),
    .sample(one_wire.sample),
    .apu(one_wire.apu),
    .drive(one_wire.drive),
    .spu(one_wire.spu)
  );

  initial apb.prdata[31:8] <= 0;

  initial begin
    automatic test_cfg uvm_test_top_cfg =
        test_cfg::type_id::create("uvm_test_top_cfg");
    uvm_test_top_cfg.apb_vif = apb;
    uvm_test_top_cfg.irq_vif = irq;
    uvm_test_top_cfg.one_wire_vif = one_wire;
    uvm_config_db#(test_cfg)::set(null, "uvm_test_top", "cfg",
                                  uvm_test_top_cfg);
    run_test();
  end
endmodule

// Copyright Ian Benz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

class test extends uvm_test;
  `uvm_component_utils(test)

  bit [7:0] clk_div;

  one_wire_master_env env;
  one_wire_master_env_cfg env_cfg;

  function new(string name, uvm_component parent);
    super.new(name, parent);
  endfunction

  virtual function void build_phase(uvm_phase phase);
    test_cfg cfg;

    if (!uvm_config_db#(test_cfg)::get(this, "", "cfg", cfg)) begin
      `uvm_fatal(get_type_name(), "Failed to get configuration (cfg)")
    end

    super.build_phase(phase);

    if (!randomize(clk_div)) begin
      `uvm_error(get_type_name(), "Failed to randomize clk_div")
    end

    env_cfg = one_wire_master_env_cfg::type_id::create("env_cfg");
    env_cfg.apb_vif = cfg.apb_vif;
    env_cfg.apb_pclk_period = 0.25us / (clk_div * 0.25 + 1);
    env_cfg.irq_vif = cfg.irq_vif;
    env_cfg.one_wire_vif = cfg.one_wire_vif;
    uvm_config_db#(one_wire_master_env_cfg)::set(this, "env", "cfg", env_cfg);
    env = one_wire_master_env::type_id::create("env", this);
  endfunction
endclass

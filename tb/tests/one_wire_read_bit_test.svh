// Copyright Ian Benz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

class one_wire_read_bit_test extends test;
  `uvm_component_utils(one_wire_read_bit_test)

  function new(string name, uvm_component parent);
    super.new(name, parent);
  endfunction

  virtual task run_phase(uvm_phase phase);
    one_wire_read_bit_sequence read_bit =
        one_wire_read_bit_sequence::type_id::create("read_bit");
    super.run_phase(phase);
    phase.raise_objection(this);
    read_bit.model = env.reg_block;
    read_bit.clk_div = clk_div;
    if (!read_bit.randomize()) begin
      `uvm_error(get_type_name(), "Failed to randomize sequence")
    end
    env.one_wire_agent.monitor.overdrive = read_bit.overdrive;
    read_bit.start(env.one_wire_agent.sequencer);
    phase.drop_objection(this);
  endtask
endclass

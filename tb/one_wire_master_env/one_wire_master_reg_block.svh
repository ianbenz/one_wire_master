// Copyright Ian Benz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

class one_wire_master_reg_block extends uvm_reg_block;
  `uvm_object_utils(one_wire_master_reg_block)

  rand one_wire_master_ctrl_reg ctrl;
  rand one_wire_master_param_reg param;
  one_wire_master_result_reg result;
  rand one_wire_master_clk_div_reg clk_div;

  local one_wire_master_reg_block_coverage coverage;

  function new(string name = "one_wire_master_reg_block");
    super.new(name, UVM_CVR_ADDR_MAP | UVM_CVR_FIELD_VALS);
  endfunction

  virtual function void build();
    one_wire_master_locked_reg_cbs ctrl_operation_en_lock;

    ctrl = one_wire_master_ctrl_reg::type_id::create("ctrl");
    ctrl.configure(.blk_parent(this));
    ctrl.build();

    param = one_wire_master_param_reg::type_id::create("param");
    param.configure(.blk_parent(this));
    param.build();

    result = one_wire_master_result_reg::type_id::create("result");
    result.configure(.blk_parent(this));
    result.build();

    clk_div = one_wire_master_clk_div_reg::type_id::create("clk_div");
    clk_div.configure(.blk_parent(this));
    clk_div.build();

    default_map = create_map(.name("default_map"), .base_addr(0), .n_bytes(1),
                             .endian(UVM_NO_ENDIAN));
    default_map.add_reg(ctrl, .offset(0));
    default_map.add_reg(param, .offset(1));
    default_map.add_reg(result, .offset(2));
    default_map.add_reg(clk_div, .offset(3));

    ctrl_operation_en_lock = one_wire_master_locked_reg_cbs::type_id::create(
        "ctrl_operation_en_lock");
    ctrl_operation_en_lock.lock_field = ctrl.operation_en;
    uvm_reg_field_cb::add(ctrl.operation, ctrl_operation_en_lock);
    uvm_reg_field_cb::add(ctrl.overdrive, ctrl_operation_en_lock);
    uvm_reg_field_cb::add(ctrl.spu, ctrl_operation_en_lock);
    uvm_reg_field_cb::add(ctrl.apu, ctrl_operation_en_lock);
    uvm_reg_field_cb::add(param.value, ctrl_operation_en_lock);
    uvm_reg_field_cb::add(clk_div.divide, ctrl_operation_en_lock);
    uvm_reg_field_cb::add(clk_div.fraction, ctrl_operation_en_lock);
  endfunction

  protected virtual function void sample(uvm_reg_addr_t offset, bit is_read,
                                         uvm_reg_map map);
    if (get_coverage(UVM_CVR_ADDR_MAP)) begin
      if (coverage == null) begin
        coverage = new;
      end
      coverage.sample(offset, is_read);
    end
  endfunction
endclass

// Copyright Ian Benz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

covergroup one_wire_master_reg_block_coverage
    with function sample(uvm_reg_addr_t offset, bit is_read);
  option.per_instance = 1;
  option.name = "one_wire_master_reg_block_coverage";

  coverpoint offset {
    bins ctrl = {0};
    bins param = {1};
    bins result = {2};
    bins clk_div = {3};
  }

  cross offset, is_read {
    ignore_bins write_result =
        binsof(offset) intersect {2} && binsof(is_read) intersect {0};
  }
endgroup

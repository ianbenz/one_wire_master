// Copyright Ian Benz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

covergroup one_wire_master_ctrl_reg_coverage
    with function sample(uvm_reg_data_t data);
  option.per_instance = 1;
  option.name = "one_wire_master_ctrl_reg_coverage";

  operation_en: coverpoint data[7];
  operation: coverpoint data[6:5] {
    bins bus_reset = {0};
    bins write_bit = {1};
    bins write_byte = {2};
    bins triplet = {3};
  }
  overdrive: coverpoint data[4];
  spu: coverpoint data[3];
  apu: coverpoint data[2];
  irq_en: coverpoint data[1];
  irq: coverpoint data[0];

  cross operation_en, operation, overdrive, spu, apu, irq_en, irq {
    ignore_bins operation_en_and_irq =
        binsof(operation_en) intersect {1} && binsof(irq) intersect {1};
  }
endgroup

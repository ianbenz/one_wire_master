// Copyright Ian Benz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

class one_wire_master_param_reg extends uvm_reg;
  `uvm_object_utils(one_wire_master_param_reg)

  rand uvm_reg_field value;

  function new(string name = "one_wire_master_param_reg");
    super.new(name, .n_bits(8), .has_coverage(UVM_NO_COVERAGE));
  endfunction

  virtual function void build();
    value = uvm_reg_field::type_id::create("value");
    value.configure(.parent(this), .size(8), .lsb_pos(0), .access("RW"),
                    .volatile(0), .reset(0), .has_reset(1), .is_rand(1),
                    .individually_accessible(1));
  endfunction
endclass

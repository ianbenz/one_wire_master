// Copyright Ian Benz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

class one_wire_bus_reset_sequence extends uvm_reg_sequence;
  `uvm_object_utils(one_wire_bus_reset_sequence)

  bit [7:0] clk_div;
  rand bit presence;
  rand bit overdrive;
  rand bit spu;
  rand bit apu;
  rand bit irq_en;

  function new(string name = "one_wire_bus_reset_sequence");
    super.new(name);
  endfunction

  virtual task body();
    one_wire_master_operation_sequence run_operation =
        one_wire_master_operation_sequence::type_id::create("run_operation");
    one_wire_reset_sequence one_wire_reset =
        one_wire_reset_sequence::type_id::create("one_wire_reset");

    if (!run_operation.randomize(param)) begin
      `uvm_error(get_type_name(), "Failed to randomize param")
    end

    run_operation.model = model;
    run_operation.clk_div = clk_div;
    run_operation.operation = one_wire_master_bus_reset_operation;
    run_operation.overdrive = overdrive;
    run_operation.spu = spu;
    run_operation.apu = apu;
    run_operation.irq_en = irq_en;
    run_operation.result = presence;

    one_wire_reset.presence = presence;
    one_wire_reset.overdrive = overdrive;

    fork
      run_operation.start(null, this);
      one_wire_reset.start(get_sequencer(), this);
    join
  endtask
endclass

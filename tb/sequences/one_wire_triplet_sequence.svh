// Copyright Ian Benz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

class one_wire_triplet_sequence extends uvm_reg_sequence;
  `uvm_object_utils(one_wire_triplet_sequence)

  bit [7:0] clk_div;
  rand bit read;
  rand bit read_complement;
  rand bit write;
  rand bit overdrive;
  rand bit spu;
  rand bit apu;
  rand bit irq_en;

  function new(string name = "one_wire_triplet_sequence");
    super.new(name);
  endfunction

  virtual task body();
    one_wire_master_operation_sequence run_operation =
        one_wire_master_operation_sequence::type_id::create("run_operation");
    one_wire_bit_sequence one_wire_bit =
        one_wire_bit_sequence::type_id::create("one_wire_bit");
    bit write_result = write;

    if (read) begin
      write_result = 1;
    end else if (read_complement) begin
      write_result = 0;
    end

    if (!run_operation.randomize(param)) begin
      `uvm_error(get_type_name(), "Failed to randomize param")
    end

    run_operation.model = model;
    run_operation.clk_div = clk_div;
    run_operation.param[0] = write;
    run_operation.operation = one_wire_master_triplet_operation;
    run_operation.overdrive = overdrive;
    run_operation.spu = spu;
    run_operation.apu = apu;
    run_operation.irq_en = irq_en;
    run_operation.result = {read, read_complement, write_result};

    one_wire_bit.overdrive = overdrive;

    fork
      run_operation.start(null, this);

      begin
        one_wire_bit.data = read;
        one_wire_bit.start(get_sequencer(), this);

        one_wire_bit.data = read_complement;
        one_wire_bit.start(get_sequencer(), this);
      end
    join
  endtask
endclass

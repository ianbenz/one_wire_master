// Copyright Ian Benz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

`include <uvm_macros.svh>

package one_wire_agent_pkg;
  import gpio_agent_pkg::*;
  import uvm_pkg::*;

  `include "one_wire_sequence_item.svh"
  `include "one_wire_monitor.svh"
  `include "one_wire_coverage.svh"
  `include "one_wire_slave_driver.svh"
  `include "one_wire_slave_agent_cfg.svh"
  `include "one_wire_slave_agent.svh"
  `include "one_wire_reset_sequence.svh"
  `include "one_wire_bit_sequence.svh"
  `include "one_wire_byte_sequence.svh"
endpackage
